﻿#include <iostream>

class Vector 
{
public:
	Vector(): x(5),y(5),z(5)
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}
	void show()
	{
		std::cout << x << ' ' << y << ' ' << z << "\n";
		std::cout << "Vector Length = " << x * x + y * y + z * z;
	}
private:
	double x;
	double y;
	double z;

};

int main()
{ 
	Vector v;
	v.show();
}

